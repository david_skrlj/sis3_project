<div id="about_bg">	
	<p>	O plagiatorstvu: za svoj projekt sem kot predlogo uprabil domačo nalogo 1. <br>
		Idejo za projekt sem dobil med popravilom razsvetljave v vratarnici našega podjetja.<br>
		Vratarnica opravlja svoje delo za pet podjetij, ki se nahajajo na območju industrijske cone Plama. Cona je ograjena vstopa pa se na glavnem vhodu, ki ga upravlja vratar/receptor. Receptor beleži obiske, usmerja goste, odpira zapornico ... poleg opravla še veliko drugih nalog.<br><br>
		Moja zamisel je taka:<br>
		1.Za znane obiskovalce naj bi vnos zahteval največ pet znakov tipkovnice in nekaj klikov miške. Znan obiskovalec pomeni, da je bil že v i.c. in ga receptor pozna. Teh obiskov je naveč. Tak način vnosa potrebuje podatkovno bazo o obiskovalcih. Zaradi vrstnega reda pri nakladanju/razkladanju, mesta (terminal) in kotaktne osebe, bi bilo zelo koristno, če bi lahko pridobil izvlečke naročilnic/dobavnic iz drugigih podatkovnih baz v podjetijh. Take obiskovalce, bi lahko knjižil reseptor sam.<br>2.Obiskovalec 'novinec' takih je v resnici zelo malo bi se vknjižil prek terminala vnos pa bi prekontroliral in potrdil vratar.<br><br>
		Ta verzija projekta je nastala v okviru projekne naloge pri predmeti Sistemi 3 - Informacijski sistemi in je neke vrste prototip.<br>
	</p>
	<h2>Video predstavitev funkcionalnosti:</h2>
	<video width="600" height="400" controls="">
		Video predstavitev funkcionalnosti.
		<source src="<?php echo base_url(); ?>video/video.mp4" type="video/mp4">
	</video> 
</div>