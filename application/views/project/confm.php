<?php echo form_open('visit/visit_confirm'); ?>
		<div class="content">
			<!-- <h1><?php var_dump($_SESSION ["evaluation_data"]); ?></h1> -->
			<div class="frame" id="t_driver">
				<h2>Voznik<br>Driver</h2>				
				<label for="fname">Ime/First name:</label><br>
				<input type="text" id="first_name" name="first_name" value = <?php echo $_SESSION ["evaluation_data"]["driver"]["first_name"]; ?>><br>
				<label for="lname">Priimek/Last name:</label><br>
				<input type="text" id="last_name" name="last_name" value = <?php echo $_SESSION ["evaluation_data"]["driver"]["last_name"]; ?>><br><br>
				<label for="lname">Država/Country:</label><br>
				<input type="text" id="driver_country" name="driver_country" value = <?php echo $_SESSION ["evaluation_data"]["driver"]["driver_country"]; ?>><br><br>
			</div>
			<div class="frame" id="truck">
				<h2>Vozilo<br>Vehicle</h2>
				<label for="regis">Registrska oznaka/Registration:</label><br>
				<input type="text" id="registy" name="registy" value = <?php echo $_SESSION ["evaluation_data"]["vehicle"]["registy"]; ?>><br>
				<label for="v_type">Tip vozila/Vehicle type:</label><br>
				<select id="vehicle_type" name="vehicle_type">
					<option value=<?php echo $_SESSION ["evaluation_data"]["vehicle"]["vehicle_type"]; ?>><?php echo $_SESSION ["evaluation_data"]["vehicle"]["vehicle_type"]; ?></option>
				    <option value="truck">Tovornjak/Truck</option>
				    <option value="van">Dostavik/Van</option>
				    <option value="car">Osebni avtomobil/Car</option>
				    <option value="other">Traktor, delovni stroj ../Other</option>
				    <option value="no_vhecile">Pešec/No vehicle</option>
				</select><br>				
				<label for="v_country">Država/Country:</label><br>
				<input type="text" id="vehicle_country" name="vehicle_country" value = <?php echo $_SESSION ["evaluation_data"]["vehicle"]["vehicle_country"]; ?>><br><br>
				<label for="v_country">Lastnik vozila/Vehicle owner:</label><br>
				<input type="text" id="vehicle_owner" name="vehicle_owner" value = <?php echo $_SESSION ["evaluation_data"]["vehicle"]["vehicle_owner"]; ?>><br><br>
			</div>
			
			<div class="frame" id="company">
				<h2>Podjetje voznika<br>Driver company</h2>
				<label for="fname">Ime podjetja/Company name:</label><br>
				<input type="text" id="conpany_name" name="conpany_name" value = <?php echo $_SESSION ["evaluation_data"]["visitor_company"]["conpany_name"]; ?>><br>
				<label for="lname">Sedež podjetja/Company h.q.:</label><br>
				<input type="text" id="conpany_hq" name="conpany_hq" value = <?php echo $_SESSION ["evaluation_data"]["visitor_company"]["conpany_hq"]; ?>><br><br>
				<label for="lname">Država/Country:</label><br>
				<input type="text" id="conpany_country" name="conpany_country" value = <?php echo $_SESSION ["evaluation_data"]["visitor_company"]["conpany_country"]; ?>><br><br>
			</div>
		</div>
		<input type="submit" value="Podatki so ok. Vpis obiskovalca v podatkovno bazo." name="submit"/><br />
	<?php echo form_close(); ?>