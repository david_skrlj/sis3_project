<?php
class Db_manipulation_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_news()
	{
		$query = $this->db->get('working_task');
		return $query->result_array();
	}

	/**/
	public function get_row_where_array($arrin, $db)
	{
		$query = $this->db->get_where($db, $arrin);
		return $query->row_array();
	}

	public function get_row_where($user, $db)
	{
		$query = $this->db->get_where($db, array('user_name' => $user));
		return $query->row_array();
	}

	public function insert_row($row, $db)
	{	
		return $this->db->insert($db , $row);			
	}

	// Insert registration data in database
	public function registration_insert($data)
	{

		// Query to check whether username already exist or not
		$condition = "user_name =" . "'" . $data['user_name'] . "'";
		$this->db->select('*');
		$this->db->from('user_login');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 0) {

			// Query to insert data in database
			$this->db->insert('user_login', $data);
			if ($this->db->affected_rows() > 0) {
				return true;
			}
		} else {
			return false;
		}
	}

	// Read data using username and password
	public function login($data, $db)
	{

		$condition = "user_name =" . "'" . $data['username'] . "' AND " . "user_password =" . "'" . $data['password'] . "'";
		$this->db->select('*');
		$this->db->from($db);
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	// Read data from database to show data in admin page
	public function read_user_information($username, $db)
	{

		$condition = "user_name =" . "'" . $username . "'";
		$this->db->select('*');
		$this->db->from($db);
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
}