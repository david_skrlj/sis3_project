<div id="main">
	<div>
		<?php
		if (isset($logout_message)) {
			echo "<div class='message'>";
			echo $logout_message;
			echo "</div>";
		}
		?>
		<?php
		if (isset($message_display)) {
			echo "<div class='message'>";
			echo $message_display;
			echo "</div>";
		}
		?>

	</div>
	<div id="login">
		<h2>Prijava 
			<?php
			if (!is_null($this->session->userdata('login_caler'))) {
			echo $this->session->userdata('login_caler');
			} 
			?>
		:</h2>
		<hr/>
		<?php echo form_open('user_authentication/signin'); ?>
		<?php
		echo "<div class='error_msg'>";
		if (isset($error_message)) {
			echo $error_message;
		}
		echo validation_errors();
		echo "</div>";
		?>
		<label>Uporabniško ime:</label>
		<input type="text" name="username" id="name" placeholder="username"/><br /><br />
		<label>Geslo :</label>
		<input type="password" name="password" id="password" placeholder="**********"/><br/><br />
		<input type="submit" value=" Login " name="submit"/><br />
		<?php echo form_close(); ?>
	</div>
</div>