<?php 
/**
 * from dn1 template
 v 4.7
 */
class Visit extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url_helper');

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		//Load database
		$this->load->model('db_manipulation_model');

		/*time and date function*/
		$this->load->helper('date');

	}

	/*helper function -> show something*/
	
	private function show_mesg($title = 'Vstop v sistem', $msg = NULL)
	{
		$data['title'] = $title;
		$data['message'] = $msg;
		
		$this->load->view('templates/header', $data);
		$this->load->view('project/welcome');
		/*echo "<h1>function show_mesg:<br>";
		var_dump($_SESSION);
		echo "</h1>";*/	    
		$this->load->view('templates/footer', $data);
	}
	
	private function show_porter_cfm()
	{
		$data['title'] = 'Receptor potrditev vnosa';
		$this->load->view('templates/header', $data);
		$this->load->view('project/confm');
		$this->load->view('templates/footer');
	}


	/*codeigniter functions*/

	function index()
	{
		$this->show_mesg();
	}

	function visitor_entry()
	{
		/*visitor section*/
		$this->form_validation->set_rules('first_name', 'Registry table', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Visitor lastname', 'trim|required');
		$this->form_validation->set_rules('driver_country', 'Visitor country', 'trim|required');

		$driver = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'driver_country' => $this->input->post('driver_country')
		);

		/*vehcile section*/
		$this->form_validation->set_rules('registy', 'Visitor name', 'trim|required');
		$this->form_validation->set_rules('vehicle_type', 'Vehicle type', 'trim|required');
		$this->form_validation->set_rules('vehicle_country', 'Vehicle country', 'trim|required');
		$this->form_validation->set_rules('vehicle_owner', 'Vehicle owner', 'trim|required');


		$vehicle = array(
			'registy' => $this->input->post('registy'),
			'vehicle_type' => $this->input->post('vehicle_type'),
			'vehicle_country' => $this->input->post('vehicle_country'),
			'vehicle_owner' => $this->input->post('vehicle_owner')
		);

		/*company section*/
		$this->form_validation->set_rules('conpany_name', 'Company name', 'trim|required');
		$this->form_validation->set_rules('conpany_hq', 'Company headquater', 'trim|required');
		$this->form_validation->set_rules('driver_country', 'Company country', 'trim|required');

		$visitor_company = array(
			'conpany_name' => $this->input->post('conpany_name'),
			'conpany_hq' => $this->input->post('conpany_hq'),
			'conpany_country' => $this->input->post('conpany_country')
		);


		$evaluation_data = array(
			'driver' => $driver,
			'vehicle' => $vehicle,
			'visitor_company' => $visitor_company
			);
		
		$this->session->set_userdata('evaluation_data', $evaluation_data);
		
		//enter data to confirm form
		$this->show_porter_cfm();
	}
	
	function visit_confirm()
	{
		$evaluation_data = $this->session->userdata("evaluation_data");
		if (is_null($evaluation_data)) {
			$this->show_mesg('Ni podatkov o obiskovalcu.', NULL);
		} else {
			/*delite enterd data from sesion*/

			$this->session->unset_userdata("evaluation_data");

			/*visitor section*/
			$this->form_validation->set_rules('first_name', 'Registry table', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Visitor lastname', 'trim|required');
			$this->form_validation->set_rules('driver_country', 'Visitor country', 'trim|required');

			$driver = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'driver_country' => $this->input->post('driver_country')
			);

			/*vehcile section*/
			$this->form_validation->set_rules('registy', 'Visitor name', 'trim|required');
			$this->form_validation->set_rules('vehicle_type', 'Vehicle type', 'trim|required');
			$this->form_validation->set_rules('vehicle_country', 'Vehicle country', 'trim|required');
			$this->form_validation->set_rules('vehicle_owner', 'Vehicle owner', 'trim|required');


			$vehicle = array(
				'registy' => $this->input->post('registy'),
				'vehicle_type' => $this->input->post('vehicle_type'),
				'vehicle_country' => $this->input->post('vehicle_country'),
				'vehicle_owner' => $this->input->post('vehicle_owner')
			);

			/*company section*/
			$this->form_validation->set_rules('conpany_name', 'Company name', 'trim|required');
			$this->form_validation->set_rules('conpany_hq', 'Company headquater', 'trim|required');
			$this->form_validation->set_rules('driver_country', 'Company country', 'trim|required');

			$visitor_company = array(
				'conpany_name' => $this->input->post('conpany_name'),
				'conpany_hq' => $this->input->post('conpany_hq'),
				'conpany_country' => $this->input->post('conpany_country')
			);

			$evaluated_data = array(
				'driver' => $driver,
				'vehicle' => $vehicle,
				'visitor_company' => $visitor_company
			);
			//$this->session->set_userdata('evaluated_data', $evaluated_data);
			//var_dump($evaluated_data);
			/*todo db write vidsitor*/
			//$this->show_porter_cfm($evaluated_data);
			//$this->show_mesg();

			/*array $evaluated_data passed trught function call*/
			$this->write_visit_db($evaluated_data);
		}
	}



	/*database managment*/
	private function write_visit_db($evaluated_data)
	{
		if (is_null($evaluated_data)) {
			
			$this->show_mesg('Napaka', 'Pomankljivi podatki vpis obiskovalca neuspešen!');
		} else {
			
			$driver = $evaluated_data['driver'];
			$vehicle = $evaluated_data['vehicle'];
			$visitor_company = $evaluated_data['visitor_company'];

			$db_driver = $this->db_manipulation_model->get_row_where_array($driver, 'driver');
			if ($db_driver == NULL) {
				$this->db_manipulation_model->insert_row($driver, 'driver');
				$db_driver = $this->db_manipulation_model->get_row_where_array($driver, 'driver');
			}

			$db_vehicle = $this->db_manipulation_model->get_row_where_array($vehicle, 'vehicle');
			if ($db_vehicle == NULL) {
				$this->db_manipulation_model->insert_row($vehicle, 'vehicle');
				$db_vehicle = $this->db_manipulation_model->get_row_where_array($vehicle, 'vehicle');
			}

			$db_visitor_company = $this->db_manipulation_model->get_row_where_array($visitor_company, 'visitor_company');
			if ($db_visitor_company == NULL) {
				$this->db_manipulation_model->insert_row($visitor_company, 'visitor_company');
				$db_visitor_company = $this->db_manipulation_model->get_row_where_array($visitor_company, 'visitor_company');
			} 
			
			
			


			/*data prepared for insert in table visits*/
			/*echo "<br>";
			var_dump($db_driver);
			echo "<br>";
			var_dump($db_vehicle);
			echo "<br>";
			var_dump($db_visitor_company);
			echo "<br><br>";*/

			/*insert visit in visit db*/
			$date = date(DATE_ATOM, time());
			
			$visit = array('time' => $date,
				'driver_id' => $db_driver['id'],
				'vehicle_id' => $db_vehicle['id'],
				'visitor_company' => $db_visitor_company['id'],
			);

			$this->db_manipulation_model->insert_row($visit, 'visits');

			$this->show_mesg('Receptor', 'Obiskovalec zabeležen.');
		}
		
	}



}	/*end class bracket!!!*/




