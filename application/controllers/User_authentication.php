<?php 
class User_authentication extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper('url_helper');

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		// Load database
		$this->load->model('login_database');
		
	}

	/* Show login page
	public function index() {
		$this->load->view('templates/header');
		$this->load->view('user_authentication/login_form');
		$this->load->view('templates/footer');
	}

	// Show registration page
	public function show() {
		$this->load->view('templates/header');
		$this->load->view('user_authentication/registration_form');
		$this->load->view('templates/footer');
	}

	// Validate and store registration data in database
	public function signup() {

		// Check validation for user input in SignUp form
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('email_value', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('user_authentication/registration_form');
		} else {
			$data = array(
				'user_name' => $this->input->post('username'),
				'user_email' => $this->input->post('email_value'),
				'user_password' => $this->input->post('password')
			);
			$result = $this->login_database->registration_insert($data);
			if ($result == TRUE) {
				$data['message_display'] = 'Registration Successfully !';
				$this->load->view('templates/header');
				$this->load->view('user_authentication/login_form', $data);
				$this->load->view('templates/footer');
			} else {
				$this->load->view('templates/header');
				$this->load->view('user_authentication/registration_form');
				$this->load->view('templates/footer');
			}
		}
	}

	public function admin(){

			if(isset($this->session->userdata['logged_in'])){
				$data['username'] = $this->session->userdata['logged_in']['username'];
				$data['email'] = $this->session->userdata['logged_in']['email'];

				$this->load->view('templates/header');
				$this->load->view('user_authentication/admin_page', $data);
				$this->load->view('templates/footer');
			}else{
				$data['message_display'] = 'Signin to view admin page!';
				$this->load->view('templates/header');
				$this->load->view('user_authentication/login_form', $data);
				$this->load->view('templates/footer');
			}
	}*/

	// Check for user login process
	public function signin() {

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);

		/*select database porter or admin carfuly with basenames!!!*/
		$db = '';
		if (!(is_null($this->session->userdata('login_caler'))))
		{
			switch ($this->session->userdata('login_caler')) {
					case 'receptor':
						//$db = 'porter_login';
						$db = 'porter_login';
						break;
					case 'administrator':
						$db = 'admin_login';
						break;
					default:
						$db = '';
						break;
			}
		}
		
		$result = $this->login_database->login($data, $db);

		//login verified
		if ($result == TRUE) {
			$username = $this->input->post('username');
			$result = $this->login_database->read_user_information($username, $db);

			$session_data = array(
				'username' => $result[0]->user_name,
				
				'level' => $result[0]->user_level,
			);
			if ((is_null($this->session->userdata('login_caler')))||($result[0]->user_level == '0')) {
				$data['title'] = 'Neveljavna prijava!';
				$this->load->view('templates/header',$data);
				$this->load->view('project/welcome');
				$this->load->view('templates/footer');
			} else {
				//login ok switch to user who request login
				switch ($this->session->userdata('login_caler')) {
					case 'receptor':
						$this->session->unset_userdata('login_caler');
						$data = array('error_message' => 'Uspešna prijava');
						$this->session->set_userdata('login_porter', $session_data);

						$data['title'] = 'Receptor';
		    			$this->load->view('templates/header', $data);
		    			$this->load->view('project/porter');
		    			$this->load->view('templates/footer');
						break;
					case 'administrator':
						$this->session->unset_userdata('login_caler');
						$data = array('error_message' => 'Uspešna prijava');
						$this->session->set_userdata('login_admin', $session_data);

						$data['title'] = 'Administrator';
		    			$this->load->view('templates/header', $data);
		   				$this->load->view('project/admin');
		    			$this->load->view('templates/footer');
						break;
					default:
						$data['title'] = 'Neveljaven prjavitelj!';
						$this->load->view('templates/header',$data);
						$this->load->view('project/welcome');
						$this->load->view('templates/footer');
						break;
				}
				
			}
			
		} else {
			$data = array(
				'error_message' => 'Neveljaven uprabnik ali geslo!'
			);
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');
		}
	}

}