<?php 
/**
 * from dn1 template
 v 4.7
 */
class Project extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url_helper');

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		//Load database
		$this->load->model('db_manipulation_model');

	}

	/*helper function -> show something*/
	
	private function show_mesg($title = 'Vstop v sistem', $msg = NULL)
	{
		$data['title'] = $title;
		$data['message'] = $msg;
		
		$this->load->view('templates/header', $data);
		$this->load->view('project/welcome');
		/*echo "<h1>function show_mesg:<br>";
		var_dump($_SESSION);
		echo "</h1>";*/	    
		$this->load->view('templates/footer', $data);
	}

	private function show_visitor()
	{
		$data['title'] = 'Obiskovalec';	
		$this->load->view('templates/header', $data);
		$this->load->view('project/visitor');
		$this->load->view('templates/footer');
	}

	private function show_porter_login()
	{
		$data['title'] = 'Receptor login';
		$this->load->view('templates/header', $data);
		$this->session->set_userdata('login_caler', 'receptor');
		$this->load->view('user_authentication/login_form.php');
		$this->load->view('templates/footer');
	}

	private function show_porter()
	{
		$data['title'] = 'Receptor';
		$this->load->view('templates/header', $data);
		$this->load->view('project/porter');
		$this->load->view('templates/footer');
	}

	private function show_porter_cfm()
	{
		$data['title'] = 'Receptor potrditev vnosa';
		$this->load->view('templates/header', $data);
		$this->load->view('project/confm');
		$this->load->view('templates/footer');
	}

	private function show_admin_login()
	{
		$data['title'] = 'Administrator login';
		$this->load->view('templates/header', $data);
		    //receptor request login -> login as receptor
		$this->session->set_userdata('login_caler', 'administrator');
		$this->load->view('user_authentication/login_form.php');
		$this->load->view('templates/footer');
	}

	private function show_admin($errmsg = NULL)
	{
		$data['error_message'] = $errmsg;
		$data['title'] = 'Administrator';
		$this->load->view('templates/header', $data);
		$this->load->view('project/admin');
		$this->load->view('templates/footer');
	}


	private function show_admin_error()
	{
		$data['title'] = 'Administrator';
		$data['error_message'] = 'Receptor z imenom že obstaja! Poizkusi z novim imenom.';
		$this->load->view('templates/header', $data);
		$this->load->view('project/admin');
		$this->load->view('templates/footer');
	}

	private function show_about()
	{
		$data['title'] = 'O projektu(about)';
	    $this->load->view('templates/header', $data);
	    $this->load->view('project/about');
	    $this->load->view('templates/footer', $data);	
	}


	/*codeigniter functions*/

	function index()
	{	
		$tit = 'Za vstop v sistem:<br>Administrator: a pass: a<br>Receptor: c pass: c';
		$msg = 'Osnovne funkcionalnosti sistema:<br>Obiskovalec se lahko vpiše in pošlje zahtevek za potrditev receptorju, ki mora seveda biti prijavlen sicer sistem ne more delovati.<br><br>Receptor lahko vnese podadke o gostu ob pomoči podatkov iz baze in si tako olajša vnos.<br><br>Receptor še enkrat preveri oboskovalčev ali svoj vnos in ga potrdi tako se obisk vknjiži v bazo obiski.<br><br>Administrator ima zaenkrat edino funkcionalnost vnos novega receptorja.<br><br>Pozor vnosna forma ne prenase praznega prostora npr KP 23 456 ni ok<br><br>todo: potrditev vnosa, receptor ...<br><br>Še linki:<br>https://www.studenti.famnit.upr.si/~89181060/sis3_project/index.php/<br>https://www.studenti.famnit.upr.si/phpmyadmin/sql.php, zbirka podatkov: 89181060<br>https://gitlab.com/david_skrlj/sis3_project.git<br>https://www.studenti.famnit.upr.si/~89181060/sis3_project/index.php/project/about (video)';
		$this->show_mesg($tit, $msg);
	}	

	
	function visitor()
	{
		if (is_null($this->session->userdata('login_porter'))) {
			
			$this->show_mesg('Obiskovalec', 'Storitev trenutno ni na voljo, poizkusite pozneje.');
		} else {
			$this->show_visitor();
		}		
	}


	function porter()
	{
		if (is_null($this->session->userdata('login_porter'))) {
			$this->show_porter_login();
		} else {
			$this->show_porter();			
		}
		
	}

	function confm()
	{
		if (is_null($this->session->userdata('login_porter'))) {
		    $this->show_porter_login();
		} else {
			if (is_null($this->session->userdata('evaluation_data'))) {
				$this->show_mesg('Napaka: potrditev vpisa.', 'Ni podatkov za potrditev.');
			} else {
				$this->show_porter_cfm();
			}			
		}
	}

	function admin()
	{
		if (is_null($this->session->userdata('login_admin'))) {
			$this->show_admin_login();		
		} else {
			$this->show_admin();			
		}
			
	}

	function add_porter()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('userlevel', 'Userlevel', 'trim|required');

		$data = array(
			'user_name' => $this->input->post('username'),
			'user_password' => $this->input->post('password'),
			'user_level' => $this->input->post('userlevel')
		);

		/*select database porter carfuly with basenames!!!*/
		$db = 'porter_login';

		$user = $this->db_manipulation_model->get_row_where($data['user_name'], $db);
		
		if ($user == NULL) {
			/*adding new user...*/
			$this->db_manipulation_model->insert_row($data, $db);
			$this->show_mesg('Administrator', 'Uspešen vnos novega receptorja.');
			//$this->show_admin();
		} else {
			$msg = 'Receptor z imenom že obstaja!<br>Poizkusi z novim imenom.';
			$this->show_admin($msg);
		}			
	}

	function about()
	{
		$this->show_about();
			
	}

	public function logout_porter() {
		$this->session->unset_userdata('login_porter', NULL);
		$this->show_porter_login();
	}
	
		public function logout_admin() {
		$this->session->unset_userdata('login_admin', NULL);
		$this->show_admin_login();
	}
}






