<html>
        <head>
                <title>Project SIS3</title>
                <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
                <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
        </head>
        <body>
                <h1><?php if(isset($title)) echo $title; ?></h1>
                <div id="header">
                        <h3 id="menu">
                                <a href="<?php echo site_url('project/index'); ?>">Vstop v sistem </a>
                                <a href="<?php echo site_url('project/visitor'); ?>">Obiskovalec </a>
                                <a href="<?php echo site_url('project/porter'); ?>">Receptor </a>
                                <a href="<?php echo site_url('project/confm'); ?>">Potrditev vpisa </a>
                                <a href="<?php echo site_url('project/admin'); ?>">Adminisrator </a>
                                <a href="<?php echo site_url('project/about'); ?>">O projektu </a>
                        </h3>
                </div>
